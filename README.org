#+TITLE: DOTFILES README
#+AUTHOR: Aimé Bertrand
#+LANGUAGE: en
#+OPTIONS: d:t toc:1 num:nil html-scripts:nil html-postamble:nil html-style:nil ^:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://macowners.club/css/gtd.css" />
#+STARTUP: indent content

* Use
You are of course free to use, copy and modify anything here.

Be careful though, some needed files are in my private repo.

* License
MIT Lisence

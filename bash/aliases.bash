## ALIASES

alias ...='cd ../../'
alias ..='cd ../'
alias cd..='cd ../'
alias ba='exit'
alias c='clear'
alias cp='cp -iv' # verbose (v) ask before replace (i)
alias grep='grep --color=yes' # grep with color
alias la='ls -Al' # show everything except for . & ..
alias ll='ls -l' # show everything except for hidden files, . & ..
alias ls='ls -hpG' # ls - prettier
alias mkdir='mkdir -pv' # verbose & create intermediate directories (p)
alias mv='mv -iv' # verbose (v) ask before replace (i)
alias vf='vifm ~'

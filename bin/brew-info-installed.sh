#!/bin/bash

# FUNCTION:
# ---------
# Create org file with my currently brew installed stuff.

# The MIT License (MIT)
# Copyright © 2023-2024 Aimé Bertrand <aime.bertrand@macowners.club>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


org_file=$HOME/Desktop/brew-installed-info.org

# put all the brew installed stuff in a temp file
brew leaves > $TMPDIR/brew-list.log
brew list --cask > $TMPDIR/brew-cask-list.log

# delete the old documentation file
[ -f $org_file ] && \
    /bin/rm $org_file

# add org title stuff
cat << _EOF_ >> $org_file
#+TITLE: INFORAMTION ABOUT BREW INSTALLED STUFF
#+CREATOR: Aimé Bertrand
#+DATE: [2023-07-26 Wed]
#+LANGUAGE: en
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://macowners.club/css/gtd.css" />
#+STARTUP: indent showall
_EOF_

echo >> $org_file
echo "* FORMULAE" >> $org_file

while read formula; do
    # echo "** ${formula}" >> $org_file
    brew info ${formula} | \
        sed '1{/^==>/s//**/;}' | \
        sed 's/* http/- http/' >> $org_file
done < $TMPDIR/brew-list.log

echo >> $org_file
echo "* CASKS" >> $org_file

while read cask; do
    # echo "** ${cask}" >> $org_file
    brew info --cask ${cask} | \
        sed '1{/^==>/s//**/;}' | \
        sed 's/* http/- http/' >> $org_file
done < $TMPDIR/brew-cask-list.log

sed -i '' 's/==>/***/g' $org_file

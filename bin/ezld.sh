#!/bin/bash

# FUNCTION:
# ---------
# Switch eza theme according to the dark or light mode in macOS.

# The MIT License (MIT)
# Copyright © 2024 Aimé Bertrand <aime.bertrand@macowners.club>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# Paths to your theme files
lightTheme="$HOME/.dotfiles/eza/theme-light.yml"
darkTheme="$HOME/.dotfiles/eza/theme-dark.yml"

# Target symlink destination in .dotfiles
targetTheme="$HOME/Library/Application Support/eza/theme.yml"

# Ensure the source directory exists
targetDirectory="$HOME/Library/Application Support/eza"
if [ ! -d "$targetDirectory" ]; then
    echo "'$targetDirectory' does not exist. Creating it..."
    mkdir -p "$targetDirectory"
fi

# Check which theme is currently in use on macOS (light or dark mode)
macOSAppearance=$(defaults read -g AppleInterfaceStyle 2>/dev/null)

# Select source file based on macOS appearance
if [[ "$macOSAppearance" == "Dark" ]]; then
    sourceTheme="$darkTheme"
    echo "Dark mode detected. Symlinking to dark theme."
else
    sourceTheme="$lightTheme"
    echo "Light mode detected or not set. Symlinking to light theme."
fi

# Remove the existing symlink or file at the target if it exists
if [ -L "$targetTheme" ] || [ -e "$targetTheme" ]; then
    echo "Removing existing target: $targetTheme"
    rm "$targetTheme"
fi

# Create the symlink
echo "Creating symlink: $sourceTheme → $targetTheme"
ln -s "$sourceTheme" "$targetTheme"
echo "Symlink successfully created!"

#!/bin/bash

# FUNCTION:
# ---------
# Script to switch to light or dark mode for macOS and most tools

# The MIT License (MIT)
# Copyright © 2023-2024 Aimé Bertrand <aime.bertrand@macowners.club>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

osAppearance=$(defaults read -g AppleInterfaceStyle 2> /dev/null)
kittyID=$(ps -ax | grep 'kitty' | head -n 1 | awk '{print $1}')
kittyBin="/Applications/kitty.app/Contents/MacOS/kitty"

# OS
/usr/bin/osascript <<EOT
tell application "System Events"
tell appearance preferences
set dark mode to not dark mode
end tell
end tell
EOT

# kitty
# case "$osAppearance" in
#     Dark) "$kittyBin" @ \
#                       --to unix:/tmp/mykitty set-colors \
#                       --all --configured \
#                       ~/.config/kitty/themes/macos-light.conf;\
#           $HOME/projects/script-collection/switch-dark-light-homepage.sh;;
#     *) "$kittyBin" @ \
#                    --to unix:/tmp/mykitty set-colors \
#                    --all --configured \
#                    ~/.config/kitty/themes/macos-dark.conf;\
#        $HOME/projects/script-collection/switch-dark-light-homepage.sh;;
# esac


# eza
$HOME/.dotfiles/bin/ezld.sh

#!/bin/bash

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title one-line
# @raycast.mode fullOutput

# Optional parameters:
# @raycast.icon 🤖
# @raycast.argument1 { "type": "text", "optional": false, "placeholder": "cmd" }

# Documentation:
# @raycast.description Prompt for a bash command and then run it
# @raycast.author Aimé Bertrand

oneLiner=$1

bash -c "$oneLiner"

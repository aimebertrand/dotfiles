#!/usr/bin/env bash

# FUNCTION:
# ---------
# Open a macOS PrefPane with:
# - fzf: https://github.com/junegunn/fzf

# The MIT License (MIT)
# Copyright © 2024 Aimé Bertrand <aime.bertrand@macowners.club>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# TODO:
# These do not work:
## Mission Control
# open "x-apple.systempreferences:com.apple.preference.expose"
## Security & Privacy - Main - Firewall")
# open "x-apple.systempreferences:com.apple.preference.security?Firewall"
## Energy Saver")
# open /System/Library/PreferencePanes/EnergySaver.prefPane
## Sharing Preference - File Sharing")
# open "x-apple.systempreferences:com.apple.preferences.sharing?Services_PersonalFileSharing"
## Sharing Preference - Bluetooth Sharing")
# open "x-apple.systempreferences:com.apple.preferences.sharing?Services_BluetoothSharing"


# Check for dependencies
for i in "fzf"; do
    command -v $i >/dev/null 2>&1 || { echo >&2 "$i is not installed."; exit 1; }
done

# Array of macOS System Preferences panes
pane=(
    "Apple ID"
    "Family Sharing"
    "General"
    "Appearance"
    "Desktop & Screen Saver"
    "Desktop & Dock"
    "Mission Control"
    "Apple Intelligence & Siri"
    "Spotlight"
    "Language & Region"
    "Notifications"
    "Internet Accounts"
    "Users & Groups"
    "Accessibility - Display"
    "Accessibility - Zoom"
    "Accessibility - VoiceOver"
    "Accessibility - Descriptions"
    "Accessibility - Captions"
    "Accessibility - Audio"
    "Accessibility - Keyboard"
    "Accessibility - Mouse & Trackpad"
    "Accessibility - Switch Control"
    "Accessibility - Dictation"
    "Screen Time"
    "Extensions"
    "Security & Privacy"
    "Security & Privacy - Main - Advanced"
    "Security & Privacy - Main - FileVault"
    "Security & Privacy - Main - Lockdown Mode"
    "Security & Privacy - Main - Firewall"
    "Security & Privacy - Privacy - Accessibility"
    "Security & Privacy - Privacy - Advertising"
    "Security & Privacy - Privacy - Full Disk Access"
    "Security & Privacy - Privacy - Analytics"
    "Security & Privacy - Privacy - App Management"
    "Security & Privacy - Privacy - Audio Capture"
    "Security & Privacy - Privacy - Automation"
    "Security & Privacy - Privacy - Bluetooth"
    "Security & Privacy - Privacy - Calendars"
    "Security & Privacy - Privacy - Camera"
    "Security & Privacy - Privacy - Contacts"
    "Security & Privacy - Privacy - Developer Tools"
    "Security & Privacy - Privacy - Files & Folders"
    "Security & Privacy - Privacy - Focus"
    "Security & Privacy - Privacy - HomeKit"
    "Security & Privacy - Privacy - Listen Event"
    "Security & Privacy - Privacy - Location Services"
    "Security & Privacy - Privacy - Media"
    "Security & Privacy - Privacy - Microphone"
    "Security & Privacy - Privacy - Motion"
    "Security & Privacy - Privacy - Nudity Detection"
    "Security & Privacy - Privacy - Passkey Access"
    "Security & Privacy - Privacy - Photos"
    "Security & Privacy - Privacy - Reminders"
    "Security & Privacy - Privacy - Remote Desktop"
    "Security & Privacy - Privacy - Screen Capture"
    "Security & Privacy - Privacy - Speech Recognition"
    "Security & Privacy - Privacy - System Services"
    "Security & Privacy - Privacy - Security"
    "Software Update"
    "Network"
    "Bluetooth"
    "Sound"
    "Printers & Scanners"
    "Keyboard"
    "Dictation"
    "Trackpad"
    "Mouse"
    "Displays"
    "Energy Saver"
    "Date & Time"
    "Sharing Preference - Screen Sharing"
    "Sharing Preference - File Sharing"
    "Sharing Preference - Printer Sharing"
    "Sharing Preference - Remote Login"
    "Sharing Preference - Remote Management"
    "Sharing Preference - Remote Apple Events"
    "Sharing Preference - Internet Sharing"
    "Sharing Preference - Bluetooth Sharing"
    "Time Machine"
    "Startup Disk"
    "Profiles"
)

# Function to open a specific System Preferences pane
open_pref_pane() {
    local selected_pane="$1"

    case "$selected_pane" in
        "Apple ID")
            open /System/Library/PreferencePanes/AppleIDPrefPane.prefPane
            ;;
        "Family Sharing")
            open /System/Library/PreferencePanes/FamilySharingPrefPane.prefPane
            ;;
        "General")
            open "x-apple.systempreferences:com.apple.preference.main"
            ;;
        "Appearance")
            open "x-apple.systempreferences:com.apple.preference.general?Appearance"
            ;;
        "Desktop & Screen Saver")
            open "x-apple.systempreferences:com.apple.preference.desktopscreeneffect"
            ;;
        "Desktop & Dock")
            open "x-apple.systempreferences:com.apple.preference.dock"
            ;;
        "Mission Control")
            open "x-apple.systempreferences:com.apple.preference.expose"
            ;;
        "Apple Intelligence & Siri")
            open "x-apple.systempreferences:com.apple.Siri-Settings.extension"
            ;;
        "Spotlight")
            open "x-apple.systempreferences:com.apple.preference.spotlight"
            ;;
        "Language & Region")
            open "x-apple.systempreferences:com.apple.Localization-Settings.extension"
            ;;
        "Notifications")
            open "x-apple.systempreferences:com.apple.preference.notifications"
            ;;
        "Internet Accounts")
            open /System/Library/PreferencePanes/InternetAccounts.prefPane
            ;;
        "Users & Groups")
            open /System/Library/PreferencePanes/Accounts.prefPane
            ;;
        "Accessibility - Display")
            open "x-apple.systempreferences:com.apple.preference.universalaccess?Seeing_Display"
            ;;
        "Accessibility - Zoom")
            open "x-apple.systempreferences:com.apple.preference.universalaccess?Seeing_Zoom"
            ;;
        "Accessibility - VoiceOver")
            open "x-apple.systempreferences:com.apple.preference.universalaccess?Seeing_VoiceOver"
            ;;
        "Accessibility - Descriptions")
            open "x-apple.systempreferences:com.apple.preference.universalaccess?Media_Descriptions"
            ;;
        "Accessibility - Captions")
            open "x-apple.systempreferences:com.apple.preference.universalaccess?Captioning"
            ;;
        "Accessibility - Audio")
            open "x-apple.systempreferences:com.apple.preference.universalaccess?Hearing"
            ;;
        "Accessibility - Keyboard")
            open "x-apple.systempreferences:com.apple.preference.universalaccess?Keyboard"
            ;;
        "Accessibility - Mouse & Trackpad")
            open "x-apple.systempreferences:com.apple.preference.universalaccess?Mouse"
            ;;
        "Accessibility - Switch Control")
            open "x-apple.systempreferences:com.apple.preference.universalaccess?Switch"
            ;;
        "Accessibility - Dictation")
            open "x-apple.systempreferences:com.apple.Accessibility-Settings.extension?Dictation"
            ;;
        "Screen Time")
            open "x-apple.systempreferences:com.apple.preference.screentime"
            ;;
        "Extensions")
            open "x-apple.systempreferences:com.apple.preference.extensions"
            ;;
        "Security & Privacy")
            open "x-apple.systempreferences:com.apple.preference.security"
            ;;
        "Security & Privacy - Main - Advanced")
            open "x-apple.systempreferences:com.apple.preference.security?Advanced"
            ;;
        "Security & Privacy - Main - FileVault")
            open "x-apple.systempreferences:com.apple.preference.security?FileVault"
            ;;
        "Security & Privacy - Main - Lockdown Mode")
            open "x-apple.systempreferences:com.apple.preference.security?LockdownMode"
            ;;
        "Security & Privacy - Main - Firewall")
            open "x-apple.systempreferences:com.apple.preference.security?Firewall"
            ;;
        "Security & Privacy - Privacy - Accessibility")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_Accessibility"
            ;;
        "Security & Privacy - Privacy - Advertising")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_Advertising"
            ;;
        "Security & Privacy - Privacy - Full Disk Access")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_AllFiles"
            ;;
        "Security & Privacy - Privacy - Analytics")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_Analytics"
            ;;
        "Security & Privacy - Privacy - App Management")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_AppBundles"
            ;;
        "Security & Privacy - Privacy - Audio Capture")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_AudioCapture"
            ;;
        "Security & Privacy - Privacy - Automation")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_Automation"
            ;;
        "Security & Privacy - Privacy - Bluetooth")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_Bluetooth"
            ;;
        "Security & Privacy - Privacy - Calendars")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_Calendars"
            ;;
        "Security & Privacy - Privacy - Camera")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_Camera"
            ;;
        "Security & Privacy - Privacy - Contacts")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_Contacts"
            ;;
        "Security & Privacy - Privacy - Developer Tools")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_DevTools"
            ;;
        "Security & Privacy - Privacy - Files & Folders")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_FilesAndFolders"
            ;;
        "Security & Privacy - Privacy - Focus")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_Focus"
            ;;
        "Security & Privacy - Privacy - HomeKit")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_HomeKit"
            ;;
        "Security & Privacy - Privacy - Listen Event")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_ListenEvent"
            ;;
        "Security & Privacy - Privacy - Location Services")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_LocationServices"
            ;;
        "Security & Privacy - Privacy - Media")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_Media"
            ;;
        "Security & Privacy - Privacy - Microphone")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_Microphone"
            ;;
        "Security & Privacy - Privacy - Motion")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_Motion"
            ;;
        "Security & Privacy - Privacy - Nudity Detection")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_NudityDetection"
            ;;
        "Security & Privacy - Privacy - Passkey Access")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_PasskeyAccess"
            ;;
        "Security & Privacy - Privacy - Photos")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_Photos"
            ;;
        "Security & Privacy - Privacy - Reminders")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_Reminders"
            ;;
        "Security & Privacy - Privacy - Remote Desktop")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_RemoteDesktop"
            ;;
        "Security & Privacy - Privacy - Screen Capture")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_ScreenCapture"
            ;;
        "Security & Privacy - Privacy - Speech Recognition")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_SpeechRecognition"
            ;;
        "Security & Privacy - Privacy - System Services")
            open "x-apple.systempreferences:com.apple.preference.security?Privacy_SystemServices"
            ;;
        "Security & Privacy - Privacy - Security")
            open "x-apple.systempreferences:com.apple.preference.security?Security"
            ;;
        "Software Update")
            open "x-apple.systempreferences:com.apple.preferences.softwareupdate"
            ;;
        "Network")
            open "x-apple.systempreferences:com.apple.preference.network"
            ;;
        "Bluetooth")
            open /System/Library/PreferencePanes/Bluetooth.prefPane
            ;;
        "Sound")
            open /System/Library/PreferencePanes/Sound.prefPane
            ;;
        "Printers & Scanners")
            open /System/Library/PreferencePanes/PrintAndScan.prefPane
            ;;
        "Keyboard")
            open /System/Library/PreferencePanes/Keyboard.prefPane
            ;;
        "Trackpad")
            open /System/Library/PreferencePanes/Trackpad.prefPane
            ;;
        "Mouse")
            open /System/Library/PreferencePanes/Mouse.prefPane
            ;;
        "Displays")
            open /System/Library/PreferencePanes/Displays.prefPane
            ;;
        "Energy Saver")
            open "x-apple.systempreferences:com.apple.Battery-Settings.extension"
            ;;
        "Date & Time")
            open "x-apple.systempreferences:com.apple.preference.datetime"
            ;;
        "Sharing Preference - Screen Sharing")
            open "x-apple.systempreferences:com.apple.preferences.sharing?Services_ScreenSharing"
            ;;
        "Sharing Preference - File Sharing")
            open "x-apple.systempreferences:com.apple.preferences.sharing?Services_PersonalFileSharing"
            ;;
        "Sharing Preference - Printer Sharing")
            open "x-apple.systempreferences:com.apple.preferences.sharing?Services_PrinterSharing"
            ;;
        "Sharing Preference - Remote Login")
            open "x-apple.systempreferences:com.apple.preferences.sharing?Services_RemoteLogin"
            ;;
        "Sharing Preference - Remote Management")
            open "x-apple.systempreferences:com.apple.preferences.sharing?Services_ARDService"
            ;;
        "Sharing Preference - Remote Apple Events")
            open "x-apple.systempreferences:com.apple.preferences.sharing?Services_RemoteAppleEvent"
            ;;
        "Sharing Preference - Internet Sharing")
            open "x-apple.systempreferences:com.apple.preferences.sharing?Internet"
            ;;
        "Sharing Preference - Bluetooth Sharing")
            open "x-apple.systempreferences:com.apple.preferences.sharing?Services_BluetoothSharing"
            ;;
        "Time Machine")
            open /System/Library/PreferencePanes/TimeMachine.prefPane
            ;;
        "Startup Disk")
            open "x-apple.systempreferences:com.apple.preference.startupdisk"
            ;;
        "Profiles")
            open "x-apple.systempreferences:com.apple.preferences.configurationprofiles"
            ;;
        *)
            echo "Unsupported preference pane"
            exit 1
            ;;
    esac
}

# Check if fzf is installed
if ! command -v fzf &> /dev/null; then
    echo "fzf is not installed. Please install fzf first."
    echo "Install it using Homebrew: brew install fzf"
    exit 1
fi

# Use fzf to select a pane
selected=$(printf '%s\n' "${pane[@]}" | fzf --layout=reverse --border --exact)

# Open the selected pane if one was chosen
if [[ -n "$selected" ]]; then
    open_pref_pane "$selected"
fi

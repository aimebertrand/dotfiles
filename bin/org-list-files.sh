#!/usr/bin/env bash

# FUNCTION:
# ---------
# List directory content into an org mode file with org relative links

# The MIT License (MIT)
# Copyright © 2024 Aimé Bertrand <aime.bertrand@macowners.club>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# Check if directory is given as argument
if [ -z "$1" ]; then
    echo "Usage: $0 <directory>"
    exit 1
fi

topDirectory="$1"
outputFile="$topDirectory/00-document-archive-listing.org"
orgDate=$(date -u +"%Y-%m-%d %a")

# Ensure the directory exists
if [ ! -d "$topDirectory" ]; then
    echo "The directory $topDirectory does not exist."
    exit 1
fi

# Create or clear the output file
cat << _EOF_ > "$outputFile"
#+TITLE: DOCUMENT ARCHIVE LISTING
#+AUTHOR: Aimé Bertrand
#+DATE: [$orgDate]
#+LANGUAGE: en
#+OPTIONS: d:t tex:verbatim ^:nil broken-links:t num:nil toc:nil
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://macowners.club/css/gtd-dark.css" />
#+STARTUP: indent overview

_EOF_

# Function to create org-mode headers and links
generate_org_structure() {
    local dir="$1"
    local level="$2"
    local base_dir="$3"

    local indent=""
    for ((i=1; i<=level; i++)); do
        indent="$indent*"
    done

    # List directories and files
    for entry in "$dir"/*; do
        if [ -d "$entry" ]; then
            local relative_dir="${entry#$base_dir/}"
            # echo "$indent [[file:$relative_dir][${relative_dir##*/}]]" >> "$outputFile"
            echo "$indent ${relative_dir##*/} – [[file:$relative_dir][open]]" >> "$outputFile"
            generate_org_structure "$entry" $((level+1)) "$base_dir"
        elif [ -f "$entry" ]; then
            local relative_file="${entry#$base_dir/}"
            # echo "- [[file:$relative_file][${relative_file##*/}]]" >> "$outputFile"
            echo "- ${relative_file##*/} – [[file:$relative_file][open]]" >> "$outputFile"
        fi
    done
}

# Start generating the structure from the top directory
generate_org_structure "$topDirectory" 1 "$topDirectory"

echo "Org mode structure has been saved to $outputFile"

open $outputFile

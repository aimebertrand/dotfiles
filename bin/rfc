#!/bin/bash

# FUNCTION:
# ---------
# Replace strings in files in directories recursively. Uses:
# - gum: https://github.com/charmbracelet/gum

# The MIT License (MIT)
# Copyright © 2023-2024 Aimé Bertrand <aime.bertrand@macowners.club>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# Check for dependencies
for i in "gum"; do
    command -v $i >/dev/null 2>&1 || { echo >&2 "$i is not installed."; exit 1; }
done

# Welcome message
gum style --align center --border double --border-foreground 4 --padding "0 2 0 2" \
    "Welcome to" \
    "$(gum style --foreground 5 --bold "rfc - rename file content")" \
    "Function: Replace strings in files in directories recursively."

# Show the current directory
gum confirm --prompt.foreground 5 \
    --selected.foreground 0 \
    --selected.background 5 \
    --unselected.foreground 7 \
    "You are in '$PWD' -- Cintinue?" || exit 0

# Prompt for old string
old_string=$(gum input --placeholder "Enter the string to replace" --prompt "Old String: ")
if [ -z "$old_string" ]; then
    gum style --foreground 1 "Error: Old string cannot be empty."
    exit 1
fi

# Prompt for new string
new_string=$(gum input --placeholder "Enter the new replacement string" --prompt "New String: ")
if [ -z "$new_string" ]; then
    gum style --foreground 1 "Error: New string cannot be empty."
    exit 1
fi

# Confirm operation
gum confirm --prompt.foreground 5 \
    --selected.foreground 0 \
    --selected.background 5 \
    --unselected.foreground 7 \
    "Replace '${old_string}' with '${new_string}' in all files recursivelly?" || exit 0

# Set locale to UTF-8 to avoid issues with character encoding
export LC_ALL=C.UTF-8

# Perform the replacement
if [ "$(uname)" = "Darwin" ]; then # macOS
    find . -type f -exec sed -i '' "s/${old_string//\//\\/}/${new_string//\//\\/}/g" {} +
else # assume GNU/Linux
    find . -type f -exec sed -i "s/${old_string//\//\\/}/${new_string//\//\\/}/g" {} +
fi

# Inform the user the operation is complete
gum style --border double --border-foreground 4 --padding "0 2 0 2" \
    "Replacement of \
$(gum style --foreground 5 --bold "${old_string}") \
with \
$(gum style --foreground 5 --bold "${new_string}") \
is complete in all files."

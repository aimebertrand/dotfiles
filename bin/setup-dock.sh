#!/usr/bin/env bash

# FUNCTION:
# ---------
# Script to add my Apps to the Dock. Uses:
# - gum: https://github.com/charmbracelet/gum

# The MIT License (MIT)
# Copyright © 2024 Aimé Bertrand <aime.bertrand@macowners.club>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# Check for dependencies
for i in "gum"; do
    command -v $i >/dev/null 2>&1 || { echo >&2 "$i is not installed."; exit 1; }
done

# Variables
currentUser=$(ls -l /dev/console | awk '{ print $3 }')

dockapps=(
    "/System/Applications/Launchpad.app"
    "/Applications/AppCleaner.app"
    "/System/Applications/Home.app"
    "/System/Applications/Mail.app"
    "/Applications/LightDark.app"
    "/System/Applications/Messages.app"
    "/System/Volumes/Preboot/Cryptexes/App/System/Applications/Safari.app"
    "/Applications/Firefox.app"
    "/Applications/iTerm.app"
    "/Applications/Emacs.app"
    "/Applications/Affinity Designer 2.app"
    "/Applications/Affinity Photo 2.app"
    "/Applications/Affinity Publisher 2.app"
    "/Applications/VSCodium.app"
    "/System/Applications/Photos.app"
    "/System/Applications/App Store.app"
    "/System/Applications/Music.app"
    "/System/Applications/Calendar.app"
    "/System/Applications/System Settings.app"
)

dockfolders=(
    "/Applications"
    "/Users/$currentUser/Downloads"
)


# Delay until User is logged in
waitForDesktop () {
    until ps aux | grep Contents/MacOS/Dock | grep -v grep &>/dev/null; do
        delay=$(( $RANDOM % 50 + 10 ))
        echo "$(date +"%Y-%m-%d %R") |  DOCK NOT RUNNING, WAITING [$delay] SECONDS"
        sleep $delay
    done
}

waitForDesktop


sudo -u "$currentUser" defaults delete /Users/$currentUser/Library/Preferences/com.apple.dock persistent-apps
sudo -u "$currentUser" defaults delete /Users/$currentUser/Library/Preferences/com.apple.dock persistent-others


# Add Apps to Dock
gum style --foreground 5 "ADDING APPS TO DOCK"
for i in "${dockapps[@]}"; do
    if [[ -a "$i" ]] ; then
        gum style --foreground 7 "$(date +"%Y-%m-%d %R")  |  Adding $(gum style --foreground 4 --bold "$i") to Dock"
        sudo -u "$currentUser" defaults write com.apple.dock persistent-apps -array-add "
<dict>
  <key>tile-data</key>
  <dict>
    <key>file-data</key>
    <dict>
      <key>_CFURLString</key>
      <string>$i</string>
      <key>_CFURLStringType</key>
      <integer>0</integer>
    </dict>
  </dict>
</dict>
"
    fi
done


# Add Apps to Dock
gum style --foreground 5 "ADDING FOLDERS TO DOCK"
for i in "${dockfolders[@]}"; do
    if [[ -a "$i" ]] ; then
        gum style --foreground 7 "$(date +"%Y-%m-%d %R")  |  Adding $(gum style --foreground 4 --bold "$i") to Dock"
        sudo -u "$currentUser" defaults write com.apple.dock persistent-others -array-add "
<dict>
  <key>tile-data</key>
  <dict>
    <key>displayas</key>
    <integer>1</integer>
    <key>showas</key>
    <integer>2</integer>
    <key>file-data</key>
    <dict>
      <key>_CFURLString</key>
      <string>$i</string>
      <key>_CFURLStringType</key>
      <integer>0</integer>
    </dict>
  </dict>
  <key>tile-type</key>
  <string>directory-tile</string>
</dict>
"
    fi
done


# Restart Dock
gum style --foreground 5 "RESTARTING DOCK"
killall Dock

#!/usr/bin/env bash

# FUNCTION:
# ---------
# Unmount Time Machine Volume after a backup

# The MIT License (MIT)
# Copyright © 2023-2024 Aimé Bertrand <aime.bertrand@macowners.club>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# Define the backup disk name
timeMachineVolume=$(tmutil destinationinfo | grep 'Name' | awk -F': ' '{print $2}')

# Trigger Time Machine backup
tmutil startbackup

# Wait for Time Machine backup to complete
while [[ $(tmutil currentphase) != "BackupNotRunning" ]]; do
    tmutil currentphase
    sleep 5
done

# Unmount the disk
/usr/bin/osascript <<EOT
tell application "Finder"
	eject (disk "$timeMachineVolume")
end tell
EOT

-----------------------------------------------
-- VARIABLES
-----------------------------------------------

-- key variable
local ctal = { "⌃", "⌥" }
local ctcm = { "⌃", "⌘" }
local alcm = { "⌥", "⌘" }
local ctalcm = { "⌃", "⌥", "⌘" }

-- other hammersponn configuration items
hs.window.animationDuration = 0 -- no animations


-----------------------------------------------
-- PACKAGE MANAGER
-----------------------------------------------

hs.loadSpoon("SpoonInstall")


-----------------------------------------------
-- RELOAD CONFIG ON WRITE
-----------------------------------------------

local function reload_config()
  hs.reload()
end

hs.hotkey.bindSpec({ ctalcm, "r" },
  function()
    reload_config()
  end
)

hs.pathwatcher.new(os.getenv("HOME") .. "/.dotfiles/hammerspoon/", reload_config):start()


-----------------------------------------------
-- WINDOW MOVEMENTS
-----------------------------------------------

-- move window 50 px to the right
hs.hotkey.bindSpec({ ctal, "right" },
  function()
    local win = hs.window.focusedWindow()
    local f = win:frame()
    f.x = f.x + 100
    win:setFrame(f)
  end
)

-- move window 50 px to the left
hs.hotkey.bindSpec({ ctal, "left" },
  function()
    local win = hs.window.focusedWindow()
    local f = win:frame()
    f.x = f.x - 100
    win:setFrame(f)
  end
)

-- move window 50 px up
hs.hotkey.bindSpec({ ctal, "up" },
  function()
    local win = hs.window.focusedWindow()
    local f = win:frame()
    f.y = f.y - 100
    win:setFrame(f)
  end
)

-- move window 50 px up
hs.hotkey.bindSpec({ ctal, "down" },
  function()
    local win = hs.window.focusedWindow()
    local f = win:frame()
    f.y = f.y + 100
    win:setFrame(f)
  end
)


-----------------------------------------------
-- WINDOW RESIZING
-----------------------------------------------

-- increase height by 100 px
hs.hotkey.bindSpec({ ctalcm, "down" }, function()
    local win = hs.window.focusedWindow()
    local f = win:frame()
    f.h = f.h + 100
    win:setFrame(f)
end)

-- decrease height by 100 px
hs.hotkey.bindSpec({ ctalcm, "up" }, function()
    local win = hs.window.focusedWindow()
    local f = win:frame()
    f.h = f.h - 100
    win:setFrame(f)
end)

-- increase width by 100 px
hs.hotkey.bindSpec({ ctalcm, "right" }, function()
    local win = hs.window.focusedWindow()
    local f = win:frame()
    f.w = f.w + 100
    win:setFrame(f)
end)

-- derease width by 100 px
hs.hotkey.bindSpec({ ctalcm, "left" }, function()
    local win = hs.window.focusedWindow()
    local f = win:frame()
    f.w = f.w - 100
    win:setFrame(f)
end)


-----------------------------------------------
-- WINDOW PLACEMENT
-----------------------------------------------

-- resize and place with grid
spoon.SpoonInstall:andUse("WindowGrid",
                          {
                            config = { gridGeometries = { { "6x4" } } },
                            hotkeys = { show_grid = { "⌘", "ü" } },
                            start = true
                          }
)

-- -- left half
-- hs.hotkey.bind(alcm, "left", function()
--                  -- size focused window to left half of display
--                  local win = hs.window.focusedWindow()
--                  local f = win:frame()
--                  local screen = win:screen()
--                  local max = screen:frame()

--                  f.x = max.x
--                  f.y = max.y
--                  f.w = max.w / 2
--                  f.h = max.h
--                  win:setFrame(f)
-- end)

-- -- right half
-- hs.hotkey.bind(alcm, "right", function()
--                  -- size focused window to right half of display
--                  local win = hs.window.focusedWindow()
--                  local f = win:frame()
--                  local screen = win:screen()
--                  local max = screen:frame()

--                  f.x = max.x + (max.w / 2)
--                  f.y = max.y
--                  f.w = max.w / 2
--                  f.h = max.h
--                  win:setFrame(f)
-- end)

-- -- top half
-- hs.hotkey.bind(alcm, "up", function()
--                  -- size focused window to top half of display
--                  local win = hs.window.focusedWindow()
--                  local f = win:frame()
--                  local screen = win:screen()
--                  local max = screen:frame()

--                  f.x = max.x
--                  f.y = max.y
--                  f.w = max.w
--                  f.h = max.h / 2
--                  win:setFrame(f)
-- end)

-- -- bottom half
-- hs.hotkey.bind(alcm, "down", function()
--                  -- size focused window to bottom half of display
--                  local win = hs.window.focusedWindow()
--                  local f = win:frame()
--                  local screen = win:screen()
--                  local max = screen:frame()

--                  f.x = max.x
--                  f.y = max.y + (max.h / 2)
--                  f.w = max.w
--                  f.h = max.h / 2
--                  win:setFrame(f)
-- end)

-- -- middle half
-- hs.hotkey.bind(alcm, "return", function()
--                  -- size focused window to middle third of display
--                  local win = hs.window.focusedWindow()
--                  local f = win:frame()
--                  local screen = win:screen()
--                  local max = screen:frame()

--                  f.x = max.x + (max.w / 4)
--                  f.y = max.y
--                  f.w = max.w / 2
--                  f.h = max.h
--                  win:setFrame(f)
-- end)

-- -- fullsize
-- hs.hotkey.bind(ctalcm, "return", function()
--                  local win = hs.window.focusedWindow()
--                  local frame = win:frame()
--                  local id = win:id()

--                  -- init table to save window state
--                  savedwin = savedwin or {}
--                  savedwin[id] = savedwin[id] or {}

--                  if (savedwin[id].maximized == nil or savedwin[id].maximized == false) then
--                    savedwin[id].frame = frame
--                    savedwin[id].maximized = true
--                    win:maximize()
--                  else
--                    savedwin[id].maximized = false
--                    win:setFrame(savedwin[id].frame)
--                    savedwin[id] = nil
--                  end
-- end)

-- fullscreen
hs.hotkey.bind(ctal, "return", function()
                 -- toggle the focused window to full screen (workspace)
                 local win = hs.window.focusedWindow()
                 win:setFullScreen(not win:isFullScreen())
end)


-----------------------------------------------
-- WINDOWS AND SPACES
-----------------------------------------------

-- uses with: https://github.com/asmagill/hs._asm.undocumented.spaces
local spaces = require("hs._asm.undocumented.spaces")

-- move current window to the space sp
function MoveWindowToSpace(sp)
  local win = hs.window.focusedWindow() -- current window
  local uuid = win:screen():spacesUUID() -- uuid for current screen
  local spaceID = spaces.layout()[uuid][sp] -- internal index for sp
  spaces.moveWindowToSpace(win:id(), spaceID) -- move window to new space
  spaces.changeToSpace(spaceID) -- follow window to new space
end

hs.hotkey.bind(alcm, '1', function() MoveWindowToSpace(1) end)
hs.hotkey.bind(alcm, '2', function() MoveWindowToSpace(2) end)
hs.hotkey.bind(alcm, '3', function() MoveWindowToSpace(3) end)
hs.hotkey.bind(alcm, '4', function() MoveWindowToSpace(4) end)
hs.hotkey.bind(alcm, '5', function() MoveWindowToSpace(5) end)


-----------------------------------------------
-- WINDOWS AND SCREENS
-----------------------------------------------

-- send window to screen by number
function moveWindowToDisplay(d)
  return function()
    local displays = hs.screen.allScreens()
    local win = hs.window.focusedWindow()
    win:moveToScreen(displays[d], false, true)
  end
end

hs.hotkey.bind(ctalcm, "1", moveWindowToDisplay(1))
hs.hotkey.bind(ctalcm, "2", moveWindowToDisplay(2))
hs.hotkey.bind(ctalcm, "3", moveWindowToDisplay(3))

-- send window to next
-- hs.hotkey.bind(ctalcm, 'n', function()
--                  -- get the focused window
--                  local win = hs.window.focusedWindow()
--                  -- get the screen where the focused window is displayed, a.k.a. current screen
--                  local screen = win:screen()
--                  -- compute the unitRect of the focused window relative to the current screen
--                  -- and move the window to the next screen setting the same unitRect
--                  win:move(win:frame():toUnitRect(screen:frame()), screen:next(), true, 0)
-- end)


-----------------------------------------------
-- CONFIG END
-----------------------------------------------

spoon.SpoonInstall:andUse("FadeLogo",
                          {
                            config = {
                              default_run = 1.0,
                            },
                            start = true
                          }
)

#!/bin/bash
#
# Copyright (c) 2021 Aimé Bertrand - aime.bertrand@macowners.club
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# read charge percent from acpi
chargepercent=$(acpi -b | grep 'Battery' | cut -d' ' -f4)
chargepercent=$(echo $chargepercent | cut -d'%' -f1)

# read charge status from acpi
chargestatus=$(acpi -b | grep 'Battery' | cut -d' ' -f3)
chargestatus=$(echo $chargestatus | cut -d',' -f1)

if [ "$chargestatus" == "Full" ]; then
    chargeindicator='F'
elif [ "$chargestatus" == "Charging" ]; then
    chargeindicator='C'
elif [ "$chargestatus" == "Discharging" ]; then
    chargeindicator='D'
elif [ "$chargestatus" == "Unknown" ]; then
    chargeindicator='U'
fi

# define colors
if [[ $chargepercent -lt 20 ]]; then
    color="%{F#bf616a}"
elif [[ $chargepercent -lt 50 ]]; then
    color="%{F#d08770}"
elif [[ $chargepercent -lt 85 ]]; then
    color="%{F#ecbe7b}"
elif [[ $chargepercent -lt 101 ]]; then
    color="%{F#a3be8c}"
fi

# echo the whole status
echo "$color $chargeindicator $chargepercent% %{F-}";

exit 0

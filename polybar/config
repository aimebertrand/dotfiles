; --------------
; POLYBAR CONFIG
; --------------
;
; MIT License
;
; Copyright (c) 2021 Aimé Bertrand - aime.bertrand@macowners.club
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in all
; copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.
;
;
; Comment:
; --------
; Documentation: https://github.com/polybar/polybar/wiki/Configuration
;
;
; Code:
; -----

[colors]
background = #2b303b
background-alt = #2f343a
foreground = #c0c5ce
foreground-alt = #dfdfdf
primary = #ecbe7b
secondary = #bf616a
alert = #bf616a


orage = #d08770
black =  #1b2229
red =  #bf616a
green =  #a3be8c
yellow =  #ecbe7b
blue =  #8fa1b3
magenta =  #b48ead
cyan =  #46d9ff
white =  #dfdfdf
plainwhite =  #ffffff


[bar/timu]
;monitor = ${env:MONITOR:HDMI-1}
width = 100%
height = 27
radius = 0.0
fixed-center = false

background = ${colors.background}
foreground = ${colors.foreground}

line-size = 3
line-color = #cc6666

border-size = 4
border-color = #00000000

padding-left = 0
padding-right = 2

module-margin-left = 1
module-margin-right = 2

; font-0 = fixed:pixelsize=10;1
font-0 = "FontAwesome:size=10;1"
font-1 = JetBrains Mono:fontformat=truetype:size=10:antialias=true;1
font-2 = JetBrains Mono:pixelsize=10;1

modules-left = i3 xwindow
modules-center = date
modules-right = backlight pulseaudio wlan battery-timu

tray-position = right
tray-padding = 2

cursor-click = pointer
cursor-scroll = ns-resize

[module/xwindow]
type = internal/xwindow
label = %title:0:30:...%
label-separator = |


[module/backlight]
type = custom/script
exec = ~/.dotfiles/i3/bin/backlight.sh
interval = 1
format-prefix = "B "
format-underline = ${colors.blue}


[module/date]
type = internal/date
interval = 1

date = %a, %d.%m.%Y
time = %H:%M
label = %date%  %time%

format-prefix-foreground = ${colors.foreground}


[module/pulseaudio]
type = internal/pulseaudio

label-volume = V %percentage%%
label-volume-foreground = ${colors.foreground}
label-volume-underline = ${colors.blue}
label-muted = mute
label-muted-foreground = ${colors.red}


[module/battery-timu]
type = custom/script
exec = ~/.dotfiles/i3/bin/timu-battery.sh
interval = 1
; format-prefix = " "
format-underline = ${colors.blue}


[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
;pseudo-transparency = false


[global/wm]
margin-top = 5
margin-bottom = 5


[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false

; focused = Active workspace on focused monitor
label-focused = %index%
label-focused-background = ${colors.background}
label-focused-underline= ${colors.blue}
label-focused-padding = 2

; unfocused = Inactive workspace on any monitor
label-unfocused = %index%
label-unfocused-foreground = ${colors.blue}
label-unfocused-padding = 2

; visible = Active workspace on unfocused monitor
label-visible = %index%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}

; urgent = Workspace with urgency hint set
label-urgent = %index%
label-urgent-background = ${colors.red}
label-urgent-foreground = ${colors.background}
label-urgent-padding = 2

; Separator in between workspaces
; label-separator = |

# POWERSHELL PROFILE FILE
# -----------------------

function prompt {
    # Get the current git branch, if any
    $hostName = (hostname)
    $gitBranch = (git branch 2>/dev/null | grep "^\*" | sed -e "s/^\*\ //")
    $gitBranch = (echo "${gitBranch} ")
    $curdir = $ExecutionContext.SessionState.Path.CurrentLocation.Path.Split("/")[-1]

    if($curdir.Length -eq 0) {
        $curdir = $ExecutionContext.SessionState.Drive.Current.Name+":/"
    }

    # Set the prompt text
    if ($gitBranch -ne "") {
        if ($gitBranch -ne " ") {
            $prompt += "`e[35m${gitBranch}`e[0m" # magenta
        }
    }
    $prompt += "`e[34mPWSH`e[0m " # blue
    $prompt += "`e[31m$hostName`e[0m" # blue
    $prompt += "`e[34m:`e[0m" # blue
    $prompt += "`e[31m$curdir`e[0m" # red
    $prompt += "`e[34m:`e[0m " # blue

    # Return the prompt text
    return $prompt
}


# Aliases & functions
Set-Alias c cls
function ba {exit}
function .. {cd ..}
function ... {cd ../..}
function .... {cd ../../..}

function ll {Get-ChildItem}


# Vi mode
Import-Module PSReadLine
Set-PSReadLineOption -EditMode Vi

# Change cursor shape in Vi Mode
# https://stackoverflow.com/q/68315479
function OnViModeChange {
    if ($args[0] -eq 'Command') {
        # Set the cursor to a blinking block.
        Write-Host -NoNewLine "`e[1 q"
    } else {
        # Set the cursor to a blinking line.
        Write-Host -NoNewLine "`e[5 q"
    }
}
Set-PSReadLineOption -ViModeIndicator Script -ViModeChangeHandler $Function:OnViModeChange
OnViModeChange

# Edit command in vim with <space>
Set-PSReadlineKeyHandler -Key " " -Function ViEditVisually -ViMode Command

timutheme = {
    # Polar Night
    '0': '#2b303b',
    '1': '#4f5b66',
    '2': '#65737e',
    '3': '#73797e',
    # Snow Storm
    '4': '#9ca0a4',
    '5': '#c0c5ce',
    '6': '#dfdfdf',
    # Frost
    '7': '#4db5bd',
    '8': '#5699Af',
    '9': '#8fa1b3',
    '10': '#5e81ac',
    # Aurora
    '11': '#bf616a',
    '12': '#d08770',
    '13': '#ebcb8b',
    '14': '#a3be8c',
    '15': '#b48ead',
    # Extra
    '16': '#232830',
}

## Background color of the completion widget category headers.
c.colors.completion.category.bg = timutheme['0']

## Bottom border color of the completion widget category headers.
c.colors.completion.category.border.bottom = timutheme['0']

## Top border color of the completion widget category headers.
c.colors.completion.category.border.top = timutheme['0']

## Foreground color of completion widget category headers.
c.colors.completion.category.fg = timutheme['6']

## Background color of the completion widget for even rows.
c.colors.completion.even.bg = timutheme['0']

## Background color of the completion widget for odd rows.
c.colors.completion.odd.bg = timutheme['16']

## Text color of the completion widget.
c.colors.completion.fg = timutheme['4']

## Background color of the selected completion item.
c.colors.completion.item.selected.bg = timutheme['3']

## Bottom border color of the selected completion item.
c.colors.completion.item.selected.border.bottom = timutheme['3']

## Top border color of the completion widget category headers.
c.colors.completion.item.selected.border.top = timutheme['3']

## Foreground color of the selected completion item.
c.colors.completion.item.selected.fg = timutheme['6']

## Foreground color of the matched text in the completion.
c.colors.completion.match.fg = timutheme['11']

## Color of the scrollbar in completion view
c.colors.completion.scrollbar.bg = timutheme['1']

## Color of the scrollbar handle in completion view.
c.colors.completion.scrollbar.fg = timutheme['5']

## Background color for the download bar.
c.colors.downloads.bar.bg = timutheme['0']

## Background color for downloads with errors.
c.colors.downloads.error.bg = timutheme['11']

## Foreground color for downloads with errors.
c.colors.downloads.error.fg = timutheme['5']

## Color gradient stop for download backgrounds.
c.colors.downloads.stop.bg = timutheme['15']

## Color gradient interpolation system for download backgrounds.
## Type: ColorSystem
## Valid values:
##   - rgb: Interpolate in the RGB color system.
##   - hsv: Interpolate in the HSV color system.
##   - hsl: Interpolate in the HSL color system.
##   - none: Don't show a gradient.
c.colors.downloads.system.bg = 'none'

## Background color for hints. Note that you can use a `rgba(...)` value
## for transparency.
c.colors.hints.bg = timutheme['10']

## Font color for hints.
c.colors.hints.fg = timutheme['0']

## Font color for the matched part of hints.
c.colors.hints.match.fg = timutheme['11']

## Background color of the keyhint widget.
c.colors.keyhint.bg = timutheme['1']

## Text color for the keyhint widget.
c.colors.keyhint.fg = timutheme['5']

## Highlight color for keys to complete the current keychain.
c.colors.keyhint.suffix.fg = timutheme['13']

## Background color of an error message.
c.colors.messages.error.bg = timutheme['11']

## Border color of an error message.
c.colors.messages.error.border = timutheme['11']

## Foreground color of an error message.
c.colors.messages.error.fg = timutheme['5']

## Background color of an info message.
c.colors.messages.info.bg = timutheme['8']

## Border color of an info message.
c.colors.messages.info.border = timutheme['8']

## Foreground color an info message.
c.colors.messages.info.fg = timutheme['6']

## Background color of a warning message.
c.colors.messages.warning.bg = timutheme['12']

## Border color of a warning message.
c.colors.messages.warning.border = timutheme['12']

## Foreground color a warning message.
c.colors.messages.warning.fg = timutheme['5']

## Background color for prompts.
c.colors.prompts.bg = timutheme['2']

# ## Border used around UI elements in prompts.
# ## Type: String
c.colors.prompts.border = '1px solid ' + timutheme['0']

## Foreground color for prompts.
c.colors.prompts.fg = timutheme['6']

## Background color for the selected item in filename prompts.
c.colors.prompts.selected.bg = timutheme['3']

## Background color of the statusbar in caret mode.
c.colors.statusbar.caret.bg = timutheme['15']

## Foreground color of the statusbar in caret mode.
c.colors.statusbar.caret.fg = timutheme['6']

## Background color of the statusbar in caret mode with a selection.
c.colors.statusbar.caret.selection.bg = timutheme['15']

## Foreground color of the statusbar in caret mode with a selection.
c.colors.statusbar.caret.selection.fg = timutheme['6']

## Background color of the statusbar in command mode.
c.colors.statusbar.command.bg = timutheme['6']

## Foreground color of the statusbar in command mode.
c.colors.statusbar.command.fg = timutheme['8']

## Background color of the statusbar in private browsing + command mode.
c.colors.statusbar.command.private.bg = timutheme['2']

## Foreground color of the statusbar in private browsing + command mode.
c.colors.statusbar.command.private.fg = timutheme['9']

## Background color of the statusbar in insert mode.
c.colors.statusbar.insert.bg = timutheme['11']

## Foreground color of the statusbar in insert mode.
c.colors.statusbar.insert.fg = timutheme['6']

## Background color of the statusbar.
c.colors.statusbar.normal.bg = timutheme['0']

## Foreground color of the statusbar.
c.colors.statusbar.normal.fg = timutheme['6']

## Background color of the statusbar in passthrough mode.
c.colors.statusbar.passthrough.bg = timutheme['10']

## Foreground color of the statusbar in passthrough mode.
c.colors.statusbar.passthrough.fg = timutheme['6']

## Background color of the statusbar in private browsing mode.
c.colors.statusbar.private.bg = timutheme['3']

## Foreground color of the statusbar in private browsing mode.
c.colors.statusbar.private.fg = timutheme['9']

## Background color of the progress bar.
c.colors.statusbar.progress.bg = timutheme['5']

## Foreground color of the URL in the statusbar on error.
c.colors.statusbar.url.error.fg = timutheme['11']

## Default foreground color of the URL in the statusbar.
c.colors.statusbar.url.fg = timutheme['6']

## Foreground color of the URL in the statusbar for hovered links.
c.colors.statusbar.url.hover.fg = timutheme['8']

## Foreground color of the URL in the statusbar on successful load
## (http).
c.colors.statusbar.url.success.http.fg = timutheme['11']

## Foreground color of the URL in the statusbar on successful load
## (https).
c.colors.statusbar.url.success.https.fg = timutheme['14']

## Foreground color of the URL in the statusbar when there's a warning.
c.colors.statusbar.url.warn.fg = timutheme['12']

## Background color of the tab bar.
c.colors.tabs.bar.bg = timutheme['16']

# ## Foreground color of selected even tabs.
c.colors.tabs.selected.even.fg = timutheme['12']

# ## Foreground color of selected odd tabs.
c.colors.tabs.selected.odd.fg = timutheme['12']

# ## Background color of selected even tabs.
c.colors.tabs.selected.even.bg = timutheme['16']

# ## Background color of selected odd tabs.
c.colors.tabs.selected.odd.bg = timutheme['16']

## Foreground color of unselected even tabs.
c.colors.tabs.even.fg = timutheme['5']

## Foreground color of unselected odd tabs.
c.colors.tabs.odd.fg = timutheme['5']

## Background color of unselected odd tabs.
c.colors.tabs.odd.bg = timutheme['0']

## Background color of unselected even tabs.
c.colors.tabs.even.bg = timutheme['0']

## Color for the tab indicator on errors.
c.colors.tabs.indicator.error = timutheme['11']

## Color gradient start for the tab indicator.
# c.colors.tabs.indicator.start = timutheme['violet']

## Color gradient end for the tab indicator.
# c.colors.tabs.indicator.stop = timutheme['orange']

## Color gradient interpolation system for the tab indicator.
## Type: ColorSystem
## Valid values:
##   - rgb: Interpolate in the RGB color system.
##   - hsv: Interpolate in the HSV color system.
##   - hsl: Interpolate in the HSL color system.
##   - none: Don't show a gradient.
c.colors.tabs.indicator.system = 'none'

## Background color for webpages if unset (or empty to use the theme's
## color)
# c.colors.webpage.bg = 'white'

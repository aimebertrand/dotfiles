"""Custom commands for ranger."""

from __future__ import (absolute_import, division, print_function)

from ranger.api.commands import Command

from subprocess import PIPE
import os


class fzfcd(Command):
    """
    Cd into directory using fd and fzf.

    Exclude some directories on macOS.
    """

    def execute(self):
        """Shell command to run."""
        command = "cd && fd -p -i -H -L -t d \
-E 'icloud/*' \
-E 'Library/*' \
-E 'Pictures/Photos Library.photoslibrary/*' \
--no-ignore-vcs --exclude '.git' \
--exclude '*.py[co]' \
--exclude '__pycache__' | fzf"
        fzf = self.fm.execute_command(command, stdout=PIPE)
        stdout, stderr = fzf.communicate()
        directory = stdout.decode('utf-8').rstrip('\n')
        directory = os.path.join(os.environ['HOME'], directory)
        self.fm.cd(directory)

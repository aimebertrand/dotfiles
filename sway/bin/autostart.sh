#!/usr/bin/bash

# FUNCTION:
# ---------
# helper for autostarting stuff in sway wm

# LICENCE:
# --------
# Copyright 2022 Aimé Bertrand

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

## AUTOTILING
~/.dotfiles/bin/autotiling &

## DUNST
killall -q dunst
/usr/bin/dunst &

## SECOND SCREEN
secondMonitor=$(swaymsg -t get_outputs | grep '"DP-1"' | cut -d'"' -f4)

if [ "$secondMonitor" == "DP-1" ]
then
    # swaymsg output DP-1 pos 0 0 res 2560x1440
    # swaymsg output eDP-1 pos 320 1440 res 1920x1080
    swaymsg output DP-1 pos 0 0 res 3840x2160
    swaymsg output eDP-1 pos 960 2160 res 1920x1080
    swaymsg workspace 1 output eDP-1
    swaymsg workspace 2 output DP-1
    swaymsg workspace 3 output DP-1
    swaymsg workspace 4 output eDP-1
    swaymsg workspace 5 output eDP-1
    swaymsg workspace 6 output DP-1
else
    swaymsg output eDP-1 pos 0 0 res 1920x1080
fi

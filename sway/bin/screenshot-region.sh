#!/usr/bin/env bash

# FUNCTION:
# ---------
# Script to take screenshots of a region in wayland
# Credit: https://github.com/rileyrg/linux-init#binswaysway-screen-menu

# LICENCE:
# --------
# Copyright 2022 Aimé Bertrand

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


DIR=${HOME}/tmp

mkdir -p "${DIR}"

FILENAME="screenshot-$(date +%F-%T).png"
region="$(slurp)"
if [ ! -z "$region" ]; then
    sleep 3
    grim -g "$region" "${DIR}"/"${FILENAME}" || exit 1
    #Create a link, so don't have to search for the newest
    ln -sf "${DIR}"/"${FILENAME}" "${DIR}"/screenshot-latest.png
    notify-send -u low "Done! see ${DIR}/screenshot-latest.png"
fi

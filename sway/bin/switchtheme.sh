# FUNCTION:
# ---------
# Script to switch between timu-spacegrey & timu-rouge

# LICENCE:
# --------
# Copyright 2021 Aimé Bertrand

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# VARIABLES
themeToSet=$(printf 'caribbean\nspacegrey\nrouge\nmacos' | rofi -dmenu)
currentTheme=$(grep 'sway-themes' ~/.dotfiles/sway/config | cut -d'-' -f3)

if [ "$currentTheme" = "caribbean" ]; then
    dunstCurrentColor1="fa87ce"
    dunstCurrentColor2="7fffd4"
    dunstCurrentColor3="161a1f"
elif [ "$currentTheme" = "spacegrey" ]; then
    dunstCurrentColor1="d08770"
    dunstCurrentColor2="5699af"
    dunstCurrentColor3="282d37"
elif [ "$currentTheme" = "rouge" ]; then
    dunstCurrentColor1="db6e8f"
    dunstCurrentColor2="6e94b9"
    dunstCurrentColor3="1f2a3f"
elif [ "$currentTheme" = "macos" ]; then
    dunstCurrentColor1="db6e8f"
    dunstCurrentColor2="6e94b9"
    dunstCurrentColor3="1f2a3f"
fi

if [ "$themeToSet" = "caribbean" ]; then
    newEmacsTheme="'timu-caribbean"
    dunstColor1ToSet="fa87ce"
    dunstColor2ToSet="7fffd4"
    dunstColor3ToSet="161a1f"
elif [ "$themeToSet" = "spacegrey" ]; then
    newEmacsTheme="'timu-spacegrey"
    dunstColor1ToSet="d08770"
    dunstColor2ToSet="5699af"
    dunstColor3ToSet="282d37"
elif [ "$themeToSet" = "rouge" ]; then
    newEmacsTheme="'timu-rouge"
    dunstColor1ToSet="db6e8f"
    dunstColor2ToSet="6e94b9"
    dunstColor3ToSet="1f2a3f"
elif [ "$themeToSet" = "macos" ]; then
    newEmacsTheme="'timu-macos"
    dunstColor1ToSet="50a5eb"
    dunstColor2ToSet="e45c9c"
    dunstColor3ToSet="232323"
fi

# EMACS
emacsclient --socket-name "/run/user/1000/emacs/server" \
            -e "(mapc #'disable-theme custom-enabled-themes)" \
            -e "(load-theme $newEmacsTheme t)"

# dunst
sed -i "s/${dunstCurrentColor1}/${dunstColor1ToSet}/" ~/.dotfiles/dunst/dunstrc
sed -i "s/${dunstCurrentColor2}/${dunstColor2ToSet}/" ~/.dotfiles/dunst/dunstrc
sed -i "s/${dunstCurrentColor3}/${dunstColor3ToSet}/" ~/.dotfiles/dunst/dunstrc

# firefox
sed -i "s/${currentTheme}/${themeToSet}/" ~/.mozilla/firefox/9bzhbd0w.default-release/chrome/userChrome.css
sed -i "s/${currentTheme}/${themeToSet}/" ~/.mozilla/firefox/9bzhbd0w.default-release/chrome/userContent.css
rm -rf ~/.mozilla/firefox/9bzhbd0w.default-release/user.js
ln -s ~/.mozilla/firefox/9bzhbd0w.default-release/chrome/timu-${themeToSet}-firefox/configuration \
   ~/.mozilla/firefox/9bzhbd0w.default-release/user.js

# gnome
gsettings set org.gnome.desktop.interface gtk-theme "timu-${themeToSet}-gnome"
gsettings set org.gnome.desktop.wm.preferences theme "timu-${themeToSet}-gnome"

# kitty
sed -i "/${currentTheme}/d" ~/.dotfiles/lkitty/kitty.conf
echo "include themes/${themeToSet}-dark.conf" >> ~/.dotfiles/lkitty/kitty.conf

# rofi
sed -i "s/timu-${currentTheme}-rofi/timu-${themeToSet}-rofi/" ~/.dotfiles/rofi/config.rasi

# qutebrowser
sed -i "s/timu-${currentTheme}/timu-${themeToSet}/" ~/.dotfiles/qutebrowser/config.py

# sway
sed -i "s/timu-${currentTheme}-sway/timu-${themeToSet}-sway/" ~/.dotfiles/sway/config

# vim
sed -i "94,106s/${currentTheme}/${themeToSet}/" ~/.dotfiles/nvim/init.vim

# waybar
mv ~/.dotfiles/waybar/style.css ~/.dotfiles/waybar/style-${currentTheme}.css && \
    mv ~/.dotfiles/waybar/style-${themeToSet}.css \
       ~/.dotfiles/waybar/style.css

# RELOAD THE SWAY SESSION
sleep 2
swaymsg reload

require("full-border"):setup()

require("smart-enter"):setup {
  open_multi = true,
}

require("mactag"):setup {
  -- Keys used to add or remove tags
  keys = {
    r = "Red",
    o = "Orange",
    y = "Yellow",
    g = "Green",
    b = "Blue",
    p = "Purple",
  },
  -- Colors used to display tags
  colors = {
    Red    = "#ff6e64",
    Orange = "#ffb350",
    Yellow = "#ffde57",
    Green  = "#6ae073",
    Blue   = "#4e9dff",
    Purple = "#cd7bf6",
  },
}

-- Show symlink path in status bar
function Status:name()
  local h = self._tab.current.hovered
  if not h then
    return ui.Line {}
  end

  local linked = ""
  if h.link_to ~= nil then
    linked = " -> " .. tostring(h.link_to)
  end
  return ui.Line(" " .. h.name .. linked)
end

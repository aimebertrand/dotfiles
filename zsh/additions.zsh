# ZSH_ADDITIONS
# -------------

## COLORS VARIABLES
BLACK='%F{0}'
RED='%F{1}'
GREEN='%F{2}'
YELLOW='%F{3}'
BLUE='%F{4}'
MAGENTA='%F{5}'
CYAN='%F{6}'
WHITE='%F{7}'
GREY='%F{8}'
PURPLE='%F{105}'
BROWN='%F{130}'
ORANGE='%F{9}'

autoload -U colors && colors

## SYNTAX HIGHLIGHTING
shPath=zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
[ -f /usr/share/$shPath ] && source /usr/share/$shPath
[ -f /usr/share/$shPath ] && source /usr/share/$shPath
[ -f /opt/homebrew/share/$shPath ] && source /opt/homebrew/share/$shPath
typeset -A ZSH_HIGHLIGHT_STYLES # overrides for the default plugin settings
ZSH_HIGHLIGHT_STYLES[path]='none' # path highlighting

## AUTOSUGGESTIONS
asPath=zsh-autosuggestions/zsh-autosuggestions.zsh
[ -f /usr/local/share/$asPath ] && source /usr/local/share/$asPath
[ -f /usr/share/$asPath ] && source /usr/share/$asPath
[ -f /opt/homebrew/share/$asPath ] && source /opt/homebrew/share/$asPath

## CUSTOM PATH
if [[ "$OSTYPE" == "darwin"* ]]; then
    if [[ $(arch) == 'arm64' ]]
    then
        path=(
            $HOME/.dotfiles/bin
            /opt/homebrew/bin
            /opt/homebrew/sbin
            /usr/local/bin
            /usr/local/sbin
            /Library/TeX/texbin
            $path
        )
        typeset -U path

        export HOMEBREW_PREFIX='/opt/homebrew'
        export HOMEBREW_CELLAR='/opt/homebrew/Cellar'
        export HOMEBREW_REPOSITORY='/opt/homebrew'
        export HOMEBREW_SHELLENV_PREFIX='/opt/homebrew'
    else
        path=(
            $HOME/.dotfiles/bin
            /usr/local/bin
            /usr/local/sbin
            /opt/homebrew/bin
            /opt/homebrew/sbin
            /Library/TeX/texbin
            $path
        )
        typeset -U path

        export HOMEBREW_PREFIX='/usr/local'
        export HOMEBREW_CELLAR='/usr/local/Cellar'
        export HOMEBREW_REPOSITORY='/usr/local/Homebrew'
        export HOMEBREW_SHELLENV_PREFIX='/usr/local'
    fi
elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
    path=(
        $HOME/.dotfiles/bin
        /home/linuxbrew/.linuxbrew/bin
        /home/linuxbrew/.linuxbrew/sbin
        /usr/local/bin
        /usr/local/sbin
        $path
    )
    typeset -U path

    export HOMEBREW_PREFIX="/home/linuxbrew/.linuxbrew";
    export HOMEBREW_CELLAR="/home/linuxbrew/.linuxbrew/Cellar";
    export HOMEBREW_REPOSITORY="/home/linuxbrew/.linuxbrew/Homebrew";
    export MANPATH="/home/linuxbrew/.linuxbrew/share/man${MANPATH+:$MANPATH}:";
    export INFOPATH="/home/linuxbrew/.linuxbrew/share/info:${INFOPATH:-}";
fi

## ZSH-COMPLETIONS
if type brew &>/dev/null; then
    fpath=($(brew --prefix)/share/zsh-completions $fpath)
    fpath=($(brew --prefix)/completions/zsh $fpath)
fi

fpath=(~/.dotfiles/zsh/completions $fpath)

autoload -Uz compinit
compinit

## HISTORY
export HISTFILE=$HOME/.dotfiles/zsh/history.zsh
export HISTSIZE=1000000
export SAVEHIST=1000000
export HISTTIMEFORMAT="[%F %T] "
setopt HIST_IGNORE_ALL_DUPS # no older duplicates
setopt HIST_REDUCE_BLANKS # remove superfluous blanks
setopt HIST_IGNORE_SPACE # ignore commands with space at the beginning
setopt SHARE_HISTORY # share history between different instances of the shell
setopt INC_APPEND_HISTORY # add cmd to the history immediately
setopt EXTENDED_HISTORY # add timestamp to the history

## AUTOCOMPLETION
autoload -Uz compinit
compinit
zstyle ':completion:*' menu select
_comp_options+=(globdots) # Include dot files
setopt auto_menu # automatically use menu completion
setopt always_to_end # move cursor to end if word had one match
# case insensitive path-completion
zstyle ':completion:*' matcher-list \
'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' \
'm:{[:lower:][:upper:]}={[:upper:][:lower:]} l:|=* r:|=*' \
'm:{[:lower:][:upper:]}={[:upper:][:lower:]} l:|=* r:|=*' \
'm:{[:lower:][:upper:]}={[:upper:][:lower:]} l:|=* r:|=*'

## CHANGE CURSOR SHAPE FOR DIFFERENT VI MODES.
# change cursor shape in iTerm2
function zle-keymap-select zle-line-init {
    case $KEYMAP in
        vicmd)      echo -ne '\e[1 q';; # block cursor
        viins|main) echo -ne '\e[5 q';; # line cursor
    esac
    zle reset-prompt
    zle -R
}

# reset to block after running command
function zle-line-finish {
    echo -ne '\e[1 q' # block cursor
}

zle -N zle-line-init
zle -N zle-line-finish
zle -N zle-keymap-select

## VI-MODE
bindkey -v
bindkey '^?' backward-delete-char # fix backspace bug when vi modes

## EDIT PROMT CMD IN $EDITOR
autoload -z edit-command-line
zle -N edit-command-line
bindkey -M vicmd ' ' edit-command-line # with <space> key

## GETTEXT STUFF
# instructions by homebrew
export LDFLAGS="-L/usr/local/opt/gettext/lib"
export CPPFLAGS="-I/usr/local/opt/gettext/include"
export LC_ALL=en_GB.UTF-8

## FZF PLUGIN
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
if [[ "$OSTYPE" == "darwin"* ]]
then
    export FZF_DEFAULT_COMMAND="fd -p -i -H -L \
-E 'icloud/*' \
-E 'Library/*' \
-E 'Pictures/Photos Library.photoslibrary/*' \
-E '.git/*'"
elif [[ "$OSTYPE" == "linux-gnu"* ]]
then
    export FZF_DEFAULT_COMMAND="fd -p -i -H -L -E '.git/*'"
fi

export FZF_COMPLETION_TRIGGER='##'

## SHOW PATH IN ITERM TITLE BAR
if [ $ITERM_SESSION_ID ]; then
    precmd() {
        echo -ne "\033]1;${PWD}\007"
    }
fi

## FOR PDF TOOLS IN EMACS
export PKG_CONFIG_PATH="/usr/local/opt/qt/lib/pkgconfig"

## FOR VTERM IN EMACS
# remove annoying '%' from the prompt in Emacs
unsetopt PROMPT_SP

## PIPENV SETTINGS
export PIPENV_VENV_IN_PROJECT=1

## CUSTOM LSCOLORS
# see: https://apple.stackexchange.com/a/282189
if [[ "$OSTYPE" == "darwin"* ]]; then
    export LSCOLORS="exfxbxdxcxgxdxbxgxcxdx"
elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
    export LS_COLORS="di=34:ln=35:so=31:pi=33:"
    export LS_COLORS=$LS_COLORS"ex=32:bd=36:cd=33:su=31:"
    export LS_COLORS=$LS_COLORS"sg=36:tw=32:ow=33"
fi

## BAT COLORS
export BAT_THEME=timu-macos

## ANALYTICS FOR HOMBREW
# https://github.com/Homebrew/brew/issues/142
export HOMEBREW_NO_ANALYTICS=1

## SSH AGENT
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    eval $(ssh-agent -s) > /dev/null
fi

## GIT
autoload -Uz vcs_info

zstyle ':vcs_info:*' enable git svn

precmd() {
    vcs_info
}

# Show count of stashed changes
function +vi-git-stash() {
    if [[ -s ${hook_com[base]}/.git/refs/stash ]] ; then
        hook_com[misc]+=" $"
    fi
}

+vi-git-untracked() {
    if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] && \
           git status --porcelain | grep -m 1 '^??' &>/dev/null
    then
        hook_com[misc]+=' ?'
    fi
}

# Enable checking for (un)staged changes, enabling use of %u and %c
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' check-for-staged-changes true

# Set custom strings for an unstaged vcs repo:
# changes (*) and staged changes (+)
zstyle ':vcs_info:*' unstagedstr ' *'
zstyle ':vcs_info:*' stagedstr ' +'

# Hooks to get stash status
zstyle ':vcs_info:git*+set-message:*' hooks git-st git-stash git-untracked

# Set the format of the Git information for vcs_info
zstyle ':vcs_info:git*' formats "${MAGENTA}%b%${YELLOW}%u%c%m "
zstyle ':vcs_info:git*' actionformats "${MAGENTA}%b%${YELLOW}%u%c|%m "

## KEYREPEAT
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    xset r rate 200 60
fi

## GNOME FOR SWAY SESSION
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    if [[ "$GDMSESSION" == "sway" ]]; then
        export XDG_CURRENT_DESKTOP=GNOME
    fi
fi

## ORBSTACK
source ~/.orbstack/shell/init.zsh 2>/dev/null || :

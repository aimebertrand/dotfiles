# ALIASES
# -------

## DEFAULT COMMANDS MORE APPEALING & USEFUL
alias ....='cd ../../../' # Go back 3 directory levels
alias ...='cd ../../' # Go back 2 directory levels
alias ..='cd ../' # Go back 1 directory level
alias ba='exit' # Exit shell
alias cdd='cd $HOME/.dotfiles/' # cd into the .dotfiles directory
alias cde='cd $HOME/.emacs.d/' # cd into the .emacs.d directory
alias cdo='cd $HOME/org/' # cd into the org directory
alias cdp='cd $HOME/projects/' # cd into the projects directory
alias cdr='cd $HOME/roam/' # cd into the roam directory
alias cp='cp -ivp' # verbose (v) und Abfrage ob ersetzen (i)
alias grep='grep --color=yes' # Color highlighting for grep
if [[ "$OSTYPE" == "darwin"* ]]; then
    # alias ls='ls -hpG'
    alias ls='eza --no-quotes'
    alias lsfs='ls -d *(.)' # list only VISIBLE files
    alias lsdfs='ls -d .[^.]*(.)' # list only HIDDEN files
    alias lsf='ls -ld *(.)' # list only VISIBLE files LONG
    alias lsdf='ls -ld .[^.]*(.)' # list only HIDDEN files LONG
    alias lsds='ls -d *(/)' # list only VISIBLE directories
    alias lsdds='ls -d .[^.]*(/)' # list only HIDDEN directories
    alias lsd='ls -ld *(/)' # list only VISIBLE directories LONG
    alias lsdd='ls -ld .[^.]*(/)' # list only HIDDEN directories LONG
    alias lslns='ls -d *(@)' # list only VISIBLE symlinks
    alias lsdlns='ls -d .[^.]*(@)' # list only HIDDEN symlinks
    alias lsln='ls -ld *(@)' # list only VISIBLE symlinks LONG
    alias lsdln='ls -ld .[^.]*(@)' # list only HIDDEN symlinks LONG
    alias lc='launchctl' # launchctl is way to long
elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
    alias ls='ls -hp --color=always'
fi
# alias la='ls -Al' # Alles bis auf . & .. anzeigen - auch versteckte Daten
alias la='eza --no-quotes -l -a -g --time-style="+%Y-%m-%d %H:%M"'
# alias ll='ls -l' # Alles bis auf . & .. und versteckte Daten anzeigen
alias ll='eza --no-quotes -l -g --time-style="+%Y-%m-%d %H:%M"'
# alias lo='ls -l *.org' # ls -al org files in the wd
alias mkd='mkdir -pv' # Verbose and parent directories for mkdir
alias mv='mv -iv' # verbose (v) und Abfrage ob ersetzen (i)
alias nano='nano -l' # Show line numbers in nano

## CUSTOM
alias kawa='caffeinate -dimsu'
alias macinfo='/usr/sbin/system_profiler SPHardwareDataType && \
/usr/sbin/system_profiler SPSoftwareDataType' # Get Hardware info
alias mutt='cd ~/Desktop && neomutt' # for attachements to land on Desktop
alias neomutt='cd ~/Desktop && neomutt' # for attachements to land on Desktop
alias cdl='curl -sS -O' # curl into file with original name
alias hist='fc -li 1' # always show the whole history and the date
alias vim='nvim'
alias vi='/usr/bin/vim'
# alias rm='trash -Fv'
alias rr='ranger --choosedir=$HOME/.dotfiles/ranger/rangerdir; \
LASTDIR=$(cat $HOME/.dotfiles/ranger/rangerdir); cd "$LASTDIR"'
if [[ "$OSTYPE" == "darwin"* ]]; then
    alias mzsh='arch -arm64 zsh'
    alias izsh='arch -x86_64 zsh'
elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
    alias lock='i3-msg exec \
"i3lock -i /usr/share/backgrounds/warty-final-ubuntu.png"'
    alias logoff='gnome-session-quit --logout --force'
    alias open='xdg-open'
fi
alias szh='source ~/.zshrc'
alias tubuntu='docker run -it registry.gitlab.com/aimebertrand/tubuntu'
alias tubunturm='docker run -it --rm registry.gitlab.com/aimebertrand/tubuntu'
alias tedora='docker run -it registry.gitlab.com/aimebertrand/tedora'
alias tedorarm='docker run -it --rm registry.gitlab.com/aimebertrand/tedora'
alias tree='eza --no-quotes -a -T'
alias tld='cat ~/.local/tld-cmd.txt | fzf  -e --reverse \
--preview "tldr {1} --color=always" \
--preview-window=right,70% | xargs tldr'

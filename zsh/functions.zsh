# FUNTIONS
# --------

## FILE AND FOLDER MANAGEMEN#
# Create a ZIP archive of a folder:
zipf() {
    zip -r "$1".zip "$1";
}


## OPENS ANY FILE IN MACOS QUICKLOOK PREVIEW
ql() {
    qlmanage -p "$*" >& /dev/null;
}


## Get help in ZSH
help() {
    bash -c "help $@" | nvim -c 'set ft=man'
}


## For when I forget sudo
# Credit: https://bezhermoso.github.io
fuck() {
    if [ $# -eq 0 ]
    then
        sudo $(fc -ln | tail -n 1)
    else
        sudo $@
    fi
}


## DETERMINE SIZE OF A FILE OR TOTAL SIZE OF A DIRECTORY
fs() {
    if du -b /dev/null > /dev/null 2>&1; then
        local arg=-sbh;
    else
        local arg=-sh;
    fi
    if [[ -n "$@" ]]; then
        du $arg -- "$@";
    else
        du $arg .[^.]* ./*;
    fi;
}


## COLORED MANPAGES
man() {
    env \
        LESS_TERMCAP_mb=$(printf "\e[1;31m") \
        LESS_TERMCAP_md=$(printf "\e[1;31m") \
        LESS_TERMCAP_me=$(printf "\e[0m") \
        LESS_TERMCAP_se=$(printf "\e[0m") \
        LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
        LESS_TERMCAP_ue=$(printf "\e[0m") \
        LESS_TERMCAP_us=$(printf "\e[1;36m") \
        man "$@"
}


## DEALING WITH .DS_STORE
dsf() {
    find . -type f -iname '.DS_Store'
}

dsd() {
    OLDIFS="$IFS"
    IFS=$'\n'
    for i in $(find . -type f -iname '.DS_Store'); do
        /bin/rm -v "$i"
    done
    IFS="$OLDIFS"
}


## OPEN STUFF WITH FZF
fa() (
    find /Applications \
         /System/Applications/Utilities \
         -maxdepth 1 -name "*.app" | fzf -e --reverse | xargs -I {} open "{}"
)

fe() (
    export FZF_DEFAULT_COMMAND="fd -p -i -H -L -tf -tl \
-E 'iCloud' \
-E 'icloud' \
-E 'Library' \
-E 'Movies' \
-E 'Music' \
-E 'Pictures/Photos Library.photoslibrary' \
-E '.git'"
    IFS=$'\n' files=($(fzf -e --reverse --preview \
"bat --theme=timu-spacegrey --color=always {}" \
                           --query="$1" --multi --select-1 --exit-0))
    [[ -n "$files" ]] && emacsclient -n -c -a '' "${files[@]}"
)

fv() (
    export FZF_DEFAULT_COMMAND="fd -p -i -H -L -tf -tl \
-E 'iCloud' \
-E 'icloud' \
-E 'Library' \
-E 'Movies' \
-E 'Music' \
-E 'Pictures/Photos Library.photoslibrary' \
-E '.git'"
    IFS=$'\n' files=($(fzf -e --reverse --preview \
"bat --theme=timu-spacegrey --color=always {}" \
                           --query="$1" --multi --select-1 --exit-0))
    [[ -n "$files" ]] && nvim "${files[@]}"
)

fo() (
    export FZF_DEFAULT_COMMAND="fd -p -i -H -L -tf -tl \
-E 'iCloud' \
-E 'icloud' \
-E 'Library' \
-E 'Movies' \
-E 'Music' \
-E 'Pictures/Photos Library.photoslibrary' \
-E '.git'"
    IFS=$'\n' files=($(fzf -e --reverse --preview \
"bat --theme=timu-spacegrey --color=always {}" \
                           --query="$1" --multi --select-1 --exit-0))
    [[ -n "$files" ]] && open "${files[@]}"
)


## CD WITH FZF
# fcd - including hidden directories
fcd() {
    export FZF_DEFAULT_COMMAND="fd -p -i -H -L -td \
-E 'iCloud' \
-E 'icloud' \
-E 'Library' \
-E 'Pictures/Photos Library.photoslibrary' \
-E '.git'"
    if [[ -n "$@" ]]
    then
        cddir=$1
    else
        cddir=$HOME
    fi
    local dir
    dir=$(cd "$cddir" && fzf -e --reverse +m) && cd "$cddir" && cd "$dir"
}


## OPEN NEW EXECUTABLE SCRIPT WITH ONE COMMAND
csc() {
    /usr/bin/touch $1 \
        && /bin/chmod +x $1 \
        && emacsclient -n -c -a '' $1
}


## GENERATE RANDOM PASSWORD
genpass() {
    /usr/bin/openssl rand -base64 9
}

## OPEN EMACS WITH DAEMON
em() {
    # Check if a frame already exists
    emacsclient -n -e "(if (> (length (frame-list)) 1) 't)" | grep -q t

    # If it doesn't create a new window (start server if not running)
    # Else, use the same window to open the file.
    if [ "$?" = "1" ]; then
        emacsclient -c -n -a "" "$@"
    else
        emacsclient -n -a "" "$@"
    fi
}

## OPEN MAILS USING FZF, MU, NOTMUCH AND VIM
nmail() {
    set -o pipefail
    mail=$(notmuch show \
                   --format=text $(notmuch search ${1:-date:1970..2050} | \
                                       fzf --reverse \
                                           --preview \
                                           "echo {} | cut -d' ' -f1 | \
cut -d':' -f2 | xargs notmuch show --format=text | \
grep 'message{' | head -n 1 | cut -d':' -f6,7 | \
sed 's/\ /\\\ /g' | xargs mu view" | \
                                       cut -d' ' -f1 | cut -d':' -f2) | \
               grep 'message{' | head -n 1 | cut -d':' -f6,7 | \
               sed 's/\ /\\\ /g' | xargs mu view) && \
        echo $mail | nvim -c 'set ft=mail' -c 'nmap q :q!<CR>'
}

nnn() {
    set -o pipefail
    mail=$(notmuch show \
                   --format=text $(notmuch search ${1:-date:1970..2050} | \
                                       fzf | cut -d' ' -f1 | cut -d':' -f2) | \
               grep -v 'part}' | \
               grep -v 'part{' | \
               grep -v 'message}' | \
               grep -v '{message' | \
               grep -v 'body}' | \
               grep -v 'body{' | \
               grep -v 'header}' | \
               grep -v 'header{') \
        && \
        echo $mail | nvim -c 'set ft=mail' -c 'nmap q :q!<CR>' -c '%s///g'
}

## MAGIT FROM CLI
magit() {
    git_root=$(git rev-parse --show-toplevel)
    emacsclient -a emacs -e "(magit-status \"${git_root}\")"
    osascript -e "tell application \"Emacs\" to activate"
}

## DRAWING BOXES
box1() {
    printf -v Bar '%*s' $((${#1} + 6)) ' '
    Bar=${Bar// /#}
    printf '%s\n## %s ##\n%s\n' "$Bar" "$1" "$Bar"
}

box2() {
    title=" $@ "
    edge=$(echo "$title" | sed 's/./*/g')
    printf "$edge\n"
    printf "\e[1;31m$title\e[0m\n"
    printf "$edge\n"
}

## ALIAS FOR CTRL+L
# Define a custom function to clear the screen when "c" + "Return" is pressed
c_clear_screen() {
    if [[ $BUFFER == "c" ]]; then
        zle clear-screen                 # Perform the clear-screen action
        BUFFER="printf '\033[H\033[2J'"  # Helper to reset the prompt
        zle accept-line
    else
        zle accept-line          # Execute the default "Return" behavior
    fi
}

# Register the custom widget
zle -N c_clear_screen

# Bind "Return" to the custom function
bindkey '^M' c_clear_screen

# For zsh-syntax-highlighting
alias c='true'

## YAZI
yz() {
    local tmp="$(mktemp -t "yazi-cwd.XXXXXX")" cwd
    yazi "$@" --cwd-file="$tmp"
    if cwd="$(command cat -- "$tmp")" && \
            [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
        builtin cd -- "$cwd"
    fi
    rm -f -- "$tmp"
}

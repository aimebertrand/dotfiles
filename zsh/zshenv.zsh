# System-wide but custom environment variables.

# On new macOS machines:
# sudo cp ~/.dotfiles/zsh/zshenv.zsh /etc/zshenv

export XDG_DATA_HOME=$HOME/.local/share
export XDG_CONFIG_HOME=$HOME/.config
export XDG_STATE_HOME=$HOME/.local/state
export XDG_CACHE_HOME=$HOME/.cache

# Custom Env Vars
export MYPROJECTS=$HOME/projects
export PRIVDOTS=$HOME/projects/pdotfiles
export PRIVEMACS=$HOME/projects/pdotemacs

# ZSHRC
# -----

## TRAMP FIX
[[ $TERM == "dumb" ]] && unsetopt zle && PS1='$ ' && return

## PRELOADS
export EDITOR=nvim
export GIT_PAGER=delta
export MANPAGER='nvim +Man!'
export KEYTIMEOUT=1
export LANG=de_DE.utf8
export MYVIMRC=$HOME/.dotfiles/vim/vimrc
export MYNVIMRC=$HOME/.dotfiles/nvim/init.lua
if [[ "$OSTYPE" == "darwin"* ]]; then
    export MY_IP=$(ifconfig en0 | grep 'inet ' | awk '{print $2}')
fi
export TERM='xterm-256color'
export VISUAL=nvim
[ -f $HOME/.dotfiles/zsh/additions.zsh ] && \
    source $HOME/.dotfiles/zsh/additions.zsh
[ -f $HOME/.dotfiles/zsh/aliases.zsh ] && \
    source $HOME/.dotfiles/zsh/aliases.zsh
[ -f $HOME/.dotfiles/zsh/functions.zsh ] && \
    source $HOME/.dotfiles/zsh/functions.zsh
[ -f $HOME/projects/pdotfiles/zsh/private.zsh ] && \
    source $HOME/projects/pdotfiles/zsh/private.zsh

## OTHER VARIABLES
thishost=$(hostname)
user='%n'
directory='%1~'
exitcode='%(?..✘ %? )' # if %? != 0, show "✘ %?"
time='%*'

## PROMPT
setopt PROMPT_SUBST

if [ -z "$SSH_TTY" ]; then
    SSHSTRING=""
else
    SSHSTRING="SSH "
fi

if [[ $EUID -ne 0 ]]; then
    PROMPT='${vcs_info_msg_0_}\
${RED}${exitcode}\
${RED}${SSHSTRING}\
${BLUE}${thishost}\
${PURPLE}%B:%b\
${BLUE}${directory}\
${PURPLE}%B:%b\
%f '
else
    PROMPT='${vcs_info_msg_0_}\
${RED}${exitcode}\
${GREEN}${SSHSTRING}\
${RED}${thishost}\
${PURPLE}%B:%b\
${RED}${directory}\
${PURPLE}%B:%b\
%f '
fi
